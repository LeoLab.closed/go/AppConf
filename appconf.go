// Package appconf project AppConf.go
package appconf

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/LeoLab/go/leotools"
)

const (
	// Version - версия
	Version = "0.4.3"
	// Date - дата сборки
	Date = "04/06/2018"
)

// ErrNotImplemented - ошибка: Не реализовано
var ErrNotImplemented = errors.New("Не реализовано")

// AppConf Основная структура
type AppConf struct {
	co   *Opts
	Data map[string]string
	Cmds []string
	args map[string]string
}

// Opts Параметры разбора конфигурации
type Opts struct {
	IniFiles []string // Абсолютные пути к .ini-файлам конфигурации, если в агрументах запуска не указан параметр --conf.file=<string>
	IniSects []string // Секции .ini-файла в порядке просмотра для поиска параметров
}

// New "Конструктор"
func New(co *Opts) (ac *AppConf, err error) {
	err = nil
	if co == nil {
		co = defaultCO()
	}
	if co.IniFiles == nil {
		co.IniFiles = make([]string, 0)
	}
	if co.IniSects == nil {
		co.IniSects = make([]string, 0)
	}
	ac = &AppConf{
		co:   co,
		Data: make(map[string]string),
		Cmds: make([]string, 0),
	}
	ac.args, ac.Cmds = leotools.ParseArgs()
	if _, ok := ac.args["ini.sects"]; ok {
		ac.co.IniSects = make([]string, 0)
		for _, sn := range strings.Split(ac.args["ini.sects"], ";") {
			ac.co.IniSects = append(ac.co.IniSects, strings.Trim(sn, " "))
			if log != nil {
				log.Debug("Добавлена секция: " + sn)
			}
		}
	} else if _, ok := ac.args["ini.sect"]; ok {
		ac.co.IniSects = []string{ac.args["ini.sect"]}
		if log != nil {
			log.Debug("Добавлена секция: " + ac.args["ini.sect"])
		}
	}

	if _, ok := ac.args["ini.files"]; ok {
		ac.co.IniFiles = make([]string, 0)
		for _, fn := range strings.Split(ac.args["ini.files"], ";") {
			ac.co.IniFiles = append(ac.co.IniFiles, strings.Trim(fn, " "))
			if log != nil {
				log.Debug("Добавлен файл: " + fn)
			}
		}
	} else if _, ok := ac.args["conf.files"]; ok {
		for _, fn := range strings.Split(ac.args["conf.files"], ";") {
			ac.co.IniFiles = append(ac.co.IniFiles, strings.Trim(fn, " "))
			if log != nil {
				log.Debug("Добавлен файл: " + fn)
			}
		}
	} else if _, ok := ac.args["ini.file"]; ok {
		ac.co.IniFiles = []string{ac.args["ini.file"]}
		if log != nil {
			log.Debug("Добавлен файл: " + ac.args["ini.file"])
		}
	} else if _, ok := ac.args["conf.file"]; ok {
		ac.co.IniFiles = []string{ac.args["conf.file"]}
		log.Debug("Добавлен файл: " + ac.args["conf.file"])
	}
	err = ac.Parse()
	return
}

// Value Получить значение параметра
func (ac *AppConf) Value(key string) string {
	if _, ok := ac.Data[key]; !ok {
		if log != nil {
			log.Debug("Значение не найдено: " + key)
		}
		return ""
	}
	if log != nil {
		log.Debug("Значение найдено: " + key + "=" + ac.Data[key])
	}
	return ac.Data[key]
}

// GetStr Получить строковое значение
func (ac *AppConf) GetStr(k string, dv string) (v string, err error) {
	if ac.Exists(k) {
		v = ac.Value(k)
	} else {
		v = dv
	}

	return v, nil
}

// GetInt Получить значение
func (ac *AppConf) GetInt(k string, dv int) (v int, err error) {
	if ac.Exists(k) {
		if v, err = strconv.Atoi(ac.Value(k)); err != nil {
			return 0, ac.eInvalidValue(k, err)
		}
	} else {
		v = dv
	}
	return v, nil
}

// GetUint -
func (ac *AppConf) GetUint(k string, dv uint) (v uint, err error) {
	if ac.Exists(k) {
		vt, err := strconv.ParseUint(ac.Value(k), 10, 32)
		if err != nil {
			return 0, ac.eInvalidValue(k, err)
		}
		v = uint(vt)
	} else {
		v = dv
	}
	return v, nil
}

// GetInt32 Получить значение
func (ac *AppConf) GetInt32(k string, dv int32) (v int32, err error) {
	if ac.Exists(k) {
		vt, err := strconv.ParseInt(ac.Value(k), 10, 32)
		if err != nil {
			return 0, ac.eInvalidValue(k, err)
		}
		v = int32(vt)
	} else {
		v = dv
	}
	return v, nil
}

// GetInt64 Получить значение
func (ac *AppConf) GetInt64(k string, dv int64) (v int64, err error) {
	if ac.Exists(k) {
		if v, err = strconv.ParseInt(ac.Value(k), 10, 64); err != nil {
			return 0, ac.eInvalidValue(k, err)
		}
	} else {
		v = dv
	}
	return v, nil
}

// GetUint64 -
func (ac *AppConf) GetUint64(k string, dv uint64) (v uint64, err error) {
	if ac.Exists(k) {
		if v, err = strconv.ParseUint(ac.Value(k), 10, 64); err != nil {
			return 0, ac.eInvalidValue(k, err)
		}
	} else {
		v = dv
	}
	return v, nil
}

// GetBool - Получить значение
func (ac *AppConf) GetBool(k string, dv bool) (v bool, err error) {
	if ac.Exists(k) {
		if ac.Value(k) != "" {
			if v, err = strconv.ParseBool(ac.Value(k)); err != nil {
				return dv, ac.eInvalidValue(k, err)
			}
		} else {
			v = true
		}
	} else {
		v = dv
	}
	return v, nil
}

// GetDuration - Получить время
func (ac *AppConf) GetDuration(k string, dv time.Duration) (v time.Duration, err error) {
	if ac.Exists(k) {
		if v, err = time.ParseDuration(ac.Value(k)); err != nil {
			return dv, ac.eInvalidValue(k, err)
		}
	} else {
		v = dv
	}
	return v, nil
}

// GetFloat32 - Получить значение
func (ac *AppConf) GetFloat32(k string, dv float32) (v float32, err error) {
	if ac.Exists(k) {
		tv, err := strconv.ParseFloat(ac.Value(k), 32)
		if err != nil {
			return 0, ac.eInvalidValue(k, err)
		}
		v = float32(tv)
	} else {
		v = dv
	}
	return v, nil
}

// GetFloat64 - Получить значение
func (ac *AppConf) GetFloat64(k string, dv float64) (v float64, err error) {
	if ac.Exists(k) {
		if v, err = strconv.ParseFloat(ac.Value(k), 64); err != nil {
			return 0, ac.eInvalidValue(k, err)
		}
	} else {
		v = dv
	}
	return v, nil
}

//Exists Есть ли параметра
func (ac *AppConf) Exists(key string, skey ...rune) bool {
	if _, ok := ac.Data[key]; ok {
		if log != nil {
			log.Debug("Параметр найден: " + key)
		}
		return true
	}
	if len(skey) > 0 {
		if _, ok := ac.Data[string(skey[0])]; ok {
			if log != nil {
				log.Debug("Параметр найден: " + string(skey[0]))
			}
			return true
		}
	}
	return false
}

// Parse - разобрать файлы и параметры
func (ac *AppConf) Parse() error {

	//= Строим список файлов >
	for _, tfn := range ac.co.IniFiles {
		if log != nil {
			log.Debug("Поиск файла: " + tfn)
		}
		if leotools.FileExists(tfn) {
			if err := ac.parseFile(tfn); err != nil {
				return errors.New("Ошибка обработки файла (" + tfn + "): " + err.Error())
			}
		}
	}
	//< Строим список файлов =

	for k, v := range ac.args {
		ac.Data[k] = v
	}
	return nil
}

func (ac *AppConf) parseFile(fName string) (err error) {
	if log != nil {
		log.Debug("Обработка файла: " + fName + ", секций: " + strconv.Itoa(len(ac.co.IniSects)))
	}
	var i *leotools.IniFile
	if i, err = leotools.NewIniFile(fName); err != nil {
		return err
	}
	if err = i.Parse(); err != nil {
		return errors.New("ini.Parse: " + err.Error())
	}

	for _, s := range ac.co.IniSects {
		if log != nil {
			log.Debug("Поиск секции: " + s)
		}
		if i.HasSection(s) {
			if log != nil {
				log.Debug("Обработка секции: " + s)
			}
			sa := i.GetSection(s)
			for k, v := range sa {
				ac.Data[k] = v
			}
		}
	}
	return nil
}

func (ac *AppConf) eInvalidValue(k string, err error) error {
	return fmt.Errorf("Неверное значение " + k + "=" + ac.Value(k) + " > " + err.Error())
}

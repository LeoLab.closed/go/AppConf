// Package appconf help
package appconf

import (
	"fmt"
)

// GetHelp Выводм встроенной справки
func GetHelp() string {
	return fmt.Sprint(`
	--ini.file=<string>
	--conf.file=<string>
		Использовать указанный файл конфигурации

	--ini.files=<string;..>
	--conf.files=<string;..>
		Использовать указанные файлы конфигурации

	--ini.sect=<string>
		Использовать указанную секцию файла(ов) конфигурации
	
	--ini.sects=<string;..>
		Использовать указанные секции файла(ов) конфигурации
`)
}

// Package appconf dbg
package appconf

import (
	"log/syslog"
)

var log *syslog.Writer

// SetDbg Установить режим отладки
func SetDbg(v bool) (err error) {
	if v {
		if log == nil {
			if log, err = syslog.New(syslog.LOG_DEBUG, "leolab/AppConf"); err != nil {
				return err
			}
		}
	} else {
		if log != nil {
			if err = log.Close(); err != nil {
				return err
			}
			log = nil
		}
	}
	return nil
}

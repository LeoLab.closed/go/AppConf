// Package appconf defaults
package appconf

import (
	"gitlab.com/LeoLab/go/leotools"
)

func defaultCO() (co *Opts) {
	// Определяем имя файла
	exeName := leotools.GetBinName()
	// Определяем папку запуска
	runPath := leotools.GetCWD()
	// Определяем папку бинарника
	exePath := leotools.GetBinDir()
	co = &Opts{
		IniFiles: make([]string, 0),
		IniSects: make([]string, 0),
	}
	co.IniFiles = append(co.IniFiles, "/etc/"+exeName+".ini", runPath+"/"+exeName+".ini", exePath+"/"+exeName+".ini")
	co.IniSects = append(co.IniSects, "global", exeName)

	return co
}
